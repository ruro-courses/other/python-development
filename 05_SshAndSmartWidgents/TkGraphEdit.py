import enum
import json
import tkinter as tk
import tkinter.colorchooser
import tkinter.font
import typing as ty

from simplified import Application, set_location


def change_color(var):
    def _callback():
        old_color = var.get()
        old_color = '#000000' if old_color is None else old_color
        _, new_color = tk.colorchooser.askcolor(old_color)
        if new_color is not None:
            var.set(new_color)

    return _callback


Pos = tuple[int, int]
Style = dict[str, ty.Union[int, str]]


class Shape(enum.IntEnum):
    Oval = enum.auto()
    Rectangle = enum.auto()


class ShapeObj(ty.NamedTuple):
    oid: int  # primary key
    origin: Pos
    pos: Pos
    shape: Shape
    style: Style


class CanvasWrapper:
    def __init__(self, canvas: tk.Canvas):
        self._canvas: tk.Canvas = canvas
        self._store: dict[int, ShapeObj] = {}

    def to_text(self) -> list[str]:
        objects = self._store.values()
        objects = sorted(objects, key=lambda obj: tuple(obj.origin) + tuple(obj.pos))
        objects = [v._asdict() for v in objects]
        for v in objects:
            v.pop('oid')
        return [json.dumps(v, sort_keys=True) for v in objects]

    def from_text(self, text: list[str]) -> list[bool]:
        for oid in list(self._store):
            self.delete_object(oid)

        results = []
        for v in text:
            if not v:
                # allow empty lines
                results.append(False)
                continue

            # noinspection PyBroadException
            try:
                self.create_object(**json.loads(v))
                results.append(False)
            except Exception:
                results.append(True)

        return results

    def create_object(
        self,
        origin: Pos,
        shape: Shape,
        style: Style,
        pos: Pos = None
    ) -> int:
        if shape in {Shape.Oval, Shape.Rectangle}:
            x0, y0 = origin
            x1, y1 = origin if pos is None else pos
            x0, x1 = sorted((x0, x1))
            y0, y1 = sorted((y0, y1))

            # noinspection PyArgumentList
            oid = {
                Shape.Oval: self._canvas.create_oval,
                Shape.Rectangle: self._canvas.create_rectangle
            }[shape](x0, y0, x1, y1, **style)

            assert oid not in self._store
            self._store[oid] = ShapeObj(oid, origin, pos, shape, style)
            return oid
        else:
            raise NotImplementedError(f"Shape {shape} is not implemented.")

    def update_object(
        self, old_oid: int,
        origin: Pos = None,
        shape: Shape = None,
        style: Style = None,
        pos: Pos = None,
    ) -> int:
        obj = self._store[old_oid]

        origin = obj.origin if origin is None else origin
        shape = obj.shape if shape is None else shape
        style = obj.style if style is None else style
        pos = obj.pos if pos is None else pos

        self.delete_object(old_oid)
        return self.create_object(origin, shape, style, pos)

    def delete_object(self, oid: int):
        self._canvas.delete(oid)
        del self._store[oid]

    def locate_object(self, mouse_pos: Pos) -> ty.Optional[int]:
        x, y = mouse_pos
        found = self._canvas.find_overlapping(x, y, x, y)
        if found:
            found = found[-1]
            assert found in self._store
            return self._store[found].oid
        else:
            return None


class TextWrapper:
    def __init__(self, text: tk.Text):
        self._text: tk.Text = text
        self._lines = []

    def clear(self):
        self._text.tag_delete(*self._text.tag_names())
        self._text.delete("0.0", "end")

    def get_text(self) -> list[str]:
        text = self._text.get("0.0", "end")
        text = text.split('\n')
        text = [e.strip() for e in text]
        return text

    def highlight_errors(self, errors: list[bool]):
        for line, error in enumerate(errors, 1):
            if not error:
                continue
            self._text.tag_add("error", f"{line}.0", f"{line}.end")
        self._text.tag_configure("error", background="red", foreground="black")

    def highlight_colors(self):
        index = "0.0"
        pat = "#[0-9a-fA-F]{6}"
        text = self._text
        colors = []

        while index := text.search(pat, f"{index}+1c", "end", regexp=True):
            color = text.get(index, f"{index}+7c")
            text.tag_add(f"color{color}", index, f"{index}+7c")
            colors.append(color)

        for color in colors:
            text.tag_configure(f"color{color}", background=color)

    def set_text(self, text: list[str]):
        self.clear()
        self._text.insert("0.0", '\n'.join(text))
        self.highlight_colors()


# noinspection PyAttributeOutsideInit
class App(Application):
    def create_other(self):
        self.lmb_down = False
        self.current_shape_oid = None

        self.size = tk.IntVar(self, 8)
        self.shape = tk.StringVar(self, next(iter(Shape)).name)
        self.mouse_coords = tk.StringVar(self, "[   0:0   ]")
        self.line_color = tk.StringVar(self, "#1f77b4")
        self.fill_color = tk.StringVar(self, "#ff7f0e")

        self.filename = tk.StringVar(self, "untitled.txt")
        self.font = tk.font.Font(self, family="Noto Mono", size=10)

    def create_widgets(self):
        self.create_other()
        f = dict(font=self.font)
        fr = tk.Frame
        bu = tk.Button

        # ряд.вес+высота:колонка.вес+ширина/гравитация
        tframe = self.TextFrame(fr, "0:0+1")
        tframe.Entry(tk.Entry, "0.0:0", **f, textvariable=self.filename)
        text = tframe.Text(tk.Text, "1:0", **f, width=52)
        text.configure(insertbackground="white", background="#2b303b")

        cframe = self.CanvasFrame(fr, "0:2+1")
        cmenu = cframe.CanvasMenu(fr, "0.0:0")
        canvas = cframe.Canvas(tk.Canvas, "1:0", background="white")

        cmenu.Ink(
            bu, "0:0.0",
            text="Ink", **f,
            command=change_color(self.line_color),
        )
        cmenu.Space1(fr, "0:1")
        cmenu.Size(
            tk.Spinbox, "0:2.0",
            foreground="black",
            background="white",
            from_=0, to=99, increment=1,
            width=2, textvariable=self.size,
        )
        cmenu.Space2(fr, "0:3")
        cmenu.Fill(
            bu, "0:4.0",
            text="Fill", **f,
            command=change_color(self.fill_color),
        )
        cmenu.Space3(fr, "0:5")
        cmenu.Indicator(
            tk.Label,
            "0:6.0",
            text="    ",
            font=tk.font.Font(self, size=0),
            highlightthickness=6,
        )
        self.line_color.trace_add("write", self.update_color)
        self.fill_color.trace_add("write", self.update_color)
        self.update_color()

        cmenu.Space4(fr, "0:7")
        cmenu.Shapes = tk.OptionMenu(cmenu, self.shape, *[e.name for e in Shape])
        cmenu.Shapes.configure(**f, width=16)
        set_location(cmenu.Shapes, "0:8")

        cmenu.Space5(fr, "0:9")
        cmenu.Coords(tk.Label, "0:10", **f, textvariable=self.mouse_coords)
        cmenu.Space6(fr, "0:11")

        menu = self.Menu(fr, "1.0:0+3")

        menu.Space1(fr, "0:0")
        menu.Im2Txt(bu, "0:1.0", **f, text="<< Image to Text <<", command=self.im2txt)
        menu.Space2(fr, "0:2")
        menu.Save(bu, "0:3.0", **f, text="Save to File", command=self.save)
        menu.Space3(fr, "0:4")

        menu.Quit(bu, "0:5.0", **f, text="Quit", command=self.quit)

        menu.Space4(fr, "0:6")
        menu.Load(bu, "0:7.0", **f, text="Load from File", command=self.load)
        menu.Space5(fr, "0:8")
        menu.Txt2Im(bu, "0:9.0", **f, text=">> Text to Image >>", command=self.txt2im)
        menu.Space6(fr, "0:10")

        self.canvas_wrapper = CanvasWrapper(canvas)
        self.text_wrapper = TextWrapper(text)
        canvas.bind('<Motion>', self.motion)

    def txt2im(self):
        errors = self.canvas_wrapper.from_text(self.text_wrapper.get_text())
        self.text_wrapper.highlight_errors(errors)

    def im2txt(self):
        self.text_wrapper.set_text(self.canvas_wrapper.to_text())

    def save(self):
        with open(self.filename.get(), 'w') as f:
            f.write('\n'.join(self.text_wrapper.get_text()))

    def load(self):
        with open(self.filename.get(), 'r') as f:
            self.text_wrapper.set_text(f.read().split('\n'))
        self.txt2im()

    def motion(self, evt):
        lmb_down = evt.state & 0x0100
        mouse_pos: Pos = (evt.x, evt.y)

        if lmb_down and not self.lmb_down:
            # Rising edge
            obj = self.canvas_wrapper.locate_object(mouse_pos)
            if obj is None:
                obj = self.canvas_wrapper.create_object(
                    origin=mouse_pos,
                    shape=Shape[self.shape.get()],
                    style=dict(
                        width=max(0, self.size.get()),
                        fill=self.fill_color.get(),
                        outline=self.line_color.get(),
                    )
                )

            self.current_shape_oid = obj
        elif self.lmb_down and not lmb_down:
            # Falling edge
            self.current_shape_oid = None
        elif lmb_down:
            # Drag
            self.current_shape_oid = self.canvas_wrapper.update_object(
                old_oid=self.current_shape_oid,
                pos=(evt.x, evt.y),
            )

        self.mouse_coords.set(f"[{mouse_pos[0]:>4}:{mouse_pos[1]:<4}]")
        self.lmb_down = lmb_down

    def update_color(self, *args):
        del args
        self.CanvasFrame.CanvasMenu.Indicator.configure(
            highlightbackground=self.line_color.get(),
            background=self.fill_color.get(),
        )


app = App(title="Graph Edit")
app.mainloop()
